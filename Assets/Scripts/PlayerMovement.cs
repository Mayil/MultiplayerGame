﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerMovement : NetworkBehaviour {

    [SerializeField]
    Transform bulletSpawnPosition;
    [SerializeField]
    GameObject bullPrefab;
    [SerializeField]
    Behaviour[] disable;


    void Start()
    {
        if (!isLocalPlayer)
        {
            foreach (Behaviour b in disable)
            {
                b.enabled=false;
            }
        }
    }
	void Update () {
        if (!isLocalPlayer)
        {
            return;
        }
 
        float x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
        float z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;

        transform.Rotate(0, x, 0);
        transform.Translate(0, 0,-z);
        if(Input.GetButtonDown("Fire1"))
        {
            CmdFire();
        }
    }

    [Command]
    void CmdFire()
    {
        GameObject bullet = (GameObject)Instantiate(bullPrefab,bulletSpawnPosition.position,bulletSpawnPosition.rotation);
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 6;
        NetworkServer.Spawn(bullet);
        Destroy(bullet, 2.0f);
    }
}
