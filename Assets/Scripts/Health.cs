﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Health : NetworkBehaviour {

    const int maxHealth = 100;
    NetworkStartPosition[] spawnPoints;
    [SyncVar(hook ="OnChangeHealth")]
    int currentHealth = maxHealth;
    public bool DestroyOnDead;
    public Text healthValue;
    void Start () {
        healthValue.text = currentHealth.ToString();
        spawnPoints = FindObjectsOfType<NetworkStartPosition>();
	}

    public void TakeDamage(int amount)
    {
        if (!isServer)
            return;
        currentHealth -= amount;
        if (currentHealth <= 0)
        {
            if (DestroyOnDead)
            {
                Destroy(gameObject);
            }
            else
            {
                currentHealth = maxHealth;
                RpcRespawn();
                Debug.Log("Dead!");
            }
        }
    }

    void OnChangeHealth (int health) {
        healthValue.text = health.ToString();
    }

    [ClientRpc]
    void RpcRespawn()
    {
        if (isLocalPlayer)
        { 
          Vector3 spawnPoint = Vector3.zero;
          if (spawnPoints != null && spawnPoints.Length > 0)
            {
                spawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)].transform.position;
            }
            transform.position = spawnPoint;
        }
    }
}
